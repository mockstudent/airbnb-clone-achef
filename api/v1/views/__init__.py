#!/usr/bin/python3
""" Blueprint for API """
from flask import Blueprint

app_views = Blueprint('app_views', __name__, url_prefix='/api/v1')

from v1.views.index import *
from v1.views.states import *
from v1.views.places import *
from v1.views.places_reviews import *
from v1.views.cities import *
from v1.views.amenities import *
from v1.views.users import *
from v1.views.places_amenities import *
